# Honey-Cracked
Please store your files under your name under the correct season.

Inventory for parts in the shop: do not take anything without asking/ adjusting the inventory tracker:
https://docs.google.com/spreadsheets/d/1TQ33Z3U-fZv1ifbLyVIeAU0HUPy8ZaNecljzhpIg2qg/edit?usp=sharing

Files should be named in the following format:
item name_material_thickness

Please track your robot parts in the document for the correct competiton:

2024:

January: https://docs.google.com/spreadsheets/d/1Tz6u993xCDknc3B_AQDtw5oDWShXo8XcdEmH_WmX-xs/edit?usp=sharing

Motorama: https://docs.google.com/spreadsheets/d/1xR1eQ__5Xo6ocr2munuHJGxr8bOkd7wlzalxmZVHFcA/edit?usp=sharing

March: https://docs.google.com/spreadsheets/d/1pyvv8Z3StTtmZpE0JScBdc4CgQOpa32Vxg4mtWfRImA/edit?usp=sharing

Teams event: https://docs.google.com/spreadsheets/d/1jx98P3kETFeNjY0tgOiqF6Afzog6x6XWVmjwL66BmDs/edit?usp=sharing

June 22nd: https://docs.google.com/spreadsheets/d/1Mfc7LVUKm1O2cFeabrPIdDm-5_V3wVosnVtwAy5Sc2Q/edit?usp=sharing

July 20th: https://docs.google.com/spreadsheets/d/1u81nh_hS5ndUVtCg1_dPwvyFgtNyWsmHgz-Z6A3GnL4/edit?usp=sharing

September 14th: https://docs.google.com/spreadsheets/d/1sgZB5rJq1d0ZGr0jTtUp_ZNXatqIyhu4FhtB1CY_k6A/edit?usp=sharing

October 26th: https://docs.google.com/spreadsheets/d/1v8eZRlZk6L25dVmQSMIHA85AZYhQ1DXa3r8TWTlRG8c/edit?usp=sharing

Championship December 14th: https://docs.google.com/spreadsheets/d/1Pbr0AH1CvJUQsgD0AYB_AhafVm4iaFqZ8vcHzK1D6x4/edit?usp=sharing

